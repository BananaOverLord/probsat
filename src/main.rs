use std::fs;
use rand::Rng;
use std::env;

/// Load a SAT problem from a file.
///
/// # Arguments
///
/// * `filename` - A string slice that holds the name of the file
///
/// # Returns
///
/// * A tuple containing the formula (list of clauses) and the number of variables
fn load_sat(filename: &str) -> (Vec<[i32; 3]>, i32) {
    let sat = fs::read_to_string(filename).expect("Failed to read the file");
    let lines = sat.split("\n");

    let mut formula: Vec<[i32; 3]> = Vec::new();
    let mut n_vars: i32 = 0;

    for line in lines {
        if line.starts_with('c') {
            // Skip comment lines
            continue;
        } else if line.starts_with('p') {
            // Process problem line
            let header: Vec<&str> = line.split_whitespace().collect();
            n_vars = header[2].parse::<i32>().unwrap();
        } else {
            // Process clause lines
            let clause: Vec<&str> = line.trim().split_whitespace().collect();

            if clause.len() >= 3 {
                formula.push([
                    clause[0].parse::<i32>().unwrap(),
                    clause[1].parse::<i32>().unwrap(),
                    clause[2].parse::<i32>().unwrap(),
                ]);
            }
        }
    }
    (formula, n_vars)
}

/// Initialize a random assignment for the variables.
///
/// # Arguments
///
/// * `n_vars` - The number of variables
///
/// # Returns
///
/// * A vector of boolean values representing the initial assignment
fn init_assignment(n_vars: i32) -> Vec<bool> {
    let mut rng = rand::thread_rng();
    (0..n_vars).map(|_| rng.gen_bool(0.5)).collect()
}

/// Evaluate a single clause against a given assignment.
///
/// # Arguments
///
/// * `clause` - The clause to be evaluated
/// * `assignment` - The current assignment of variables
///
/// # Returns
///
/// * A boolean value indicating if the clause is satisfied
fn eval_clause(clause: [i32; 3], assignment: &Vec<bool>) -> bool {
    clause.iter().any(|&literal| {
        let var_index = literal.abs() as usize - 1;
        (literal > 0) == assignment[var_index]
    })
}

/// Calculate the probability of flipping a literal.
///
/// # Arguments
///
/// * `literal` - The literal to evaluate
/// * `formula` - The SAT problem
/// * `assignment` - The current assignment of variables
///
/// # Returns
///
/// * A float value representing the probability of flipping the literal
fn prob(literal: i32, formula: &Vec<[i32; 3]>, assignment: &Vec<bool>) -> f32 {
    let mut break_count = 0;
    let mut make_count = 0;

    let var_index = (literal.abs() - 1) as usize;
    let new_assignment = !assignment[var_index];

    for clause in formula {
        let original = eval_clause(*clause, assignment);
        let new_result = eval_clause(*clause, &assignment.iter().enumerate().map(|(i, &val)| if i == var_index { new_assignment } else { val }).collect());

        if original && !new_result {
            break_count += 1;
        } else if !original && new_result {
            make_count += 1;
        }
    }

    let a = f32::powf(make_count as f32, 0.0);
    let b = f32::powf((break_count as f32) + 0.0001, 2.3);
    a / b
}

/// Evaluate the entire formula against a given assignment.
///
/// # Arguments
///
/// * `formula` - The SAT problem
/// * `assignment` - The current assignment of variables
///
/// # Returns
///
/// * A tuple of a boolean indicating if the formula is satisfied and a vector of results for each clause
fn eval_formula(formula: &Vec<[i32; 3]>, assignment: &Vec<bool>) -> (bool, Vec<bool>) {
    let clauses_result: Vec<bool> = formula.iter().map(|&clause| eval_clause(clause, assignment)).collect();
    let result = clauses_result.iter().all(|&val| val);
    (result, clauses_result)
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let (formula, n_vars) = load_sat(&args[1]);
    let max_tries = args[3].parse::<i32>().unwrap(); 
    let max_flips = args[2].parse::<i32>().unwrap();

    for _ in 0..max_tries {
        let mut assignment = init_assignment(n_vars);

        for flip in 0..max_flips {
            let (result, clauses_results) = eval_formula(&formula, &assignment);

            if result {
                println!("{}", flip);
                break;
            }

            // Choose a random unsatisfied clause
            let false_indices: Vec<_> = clauses_results.iter().enumerate().filter(|&(_, &value)| !value).map(|(index, _)| index).collect();
            let random_index = rand::thread_rng().gen_range(0..false_indices.len());
            let random_clause = formula[false_indices[random_index]];

            // Calculate probabilities for flipping each literal in the clause
            let mut probs: Vec<f32> = random_clause.iter().map(|&literal| prob(literal, &formula, &assignment)).collect();
            let prob_sum: f32 = probs.iter().sum();

            // Randomly decide which literal to flip based on calculated probabilities
            let random_prob: f32 = rand::thread_rng().gen_range(0.0..prob_sum);
            let mut accumulated_prob = 0.0;

            for (i, &literal) in random_clause.iter().enumerate() {
                accumulated_prob += probs[i];
                if random_prob < accumulated_prob {
                    let var_index = (literal.abs() - 1) as usize;
                    assignment[var_index] = !assignment[var_index];
                    break;
                }
            }
        }

        let (result, _) = eval_formula(&formula, &assignment);
        if !result {
            println!("{}", max_flips);
        }
    }
}

